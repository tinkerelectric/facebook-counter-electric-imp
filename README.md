# README #

Physical Facebook Like Counter - By Tinker Electric / John Missikos

Display the likes of any Facebook Page beautifully on your desk or shop window.

This is a Facebook Like counter based around the Electric Imp, April board and Sparkfun Open Segment display.

### What is this repository for? ###

* Version 1


### How do I get set up? ###

* Purchase BOM items
* Wire to specifications
* Get FB developer account
* Obtain App codes
* Get Imp login
* Deploy Imp Code
* Enjoy + Sip coffee

### Contribution guidelines ###

* Open to code improvements

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact